# Sempre IT - Test #

I've developed this app using the following resources and libraries:

### Programming Language and Pattern ###

* Architecture Pattern MVP (Model - View - Presenter)
* Kotlin Programming Language

### Libraries ###

* AndroidX/UI Libraries & Material Design
* Glide v.4.9.0 (Image Loading)
* Retrofit v.2.5.0 (Network Requests and Json Parsing)

### Tests ###

* JUnit
* Espresso

### Author: Eduardo Viana Pessoa ###