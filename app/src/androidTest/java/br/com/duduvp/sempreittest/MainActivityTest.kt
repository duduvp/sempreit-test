package br.com.duduvp.sempreittest

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import br.com.duduvp.sempreittest.ui.main.MainActivity
import org.hamcrest.Matchers.not
import org.hamcrest.Matchers.startsWith
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
@LargeTest
class MainActivityTest {

    @Rule
    @JvmField
    val rule = IntentsTestRule(MainActivity::class.java)

    @Test
    fun testActivityViews() {
        onView(withId(R.id.imgWelcome)).check(matches(isEnabled()))
        onView(withId(R.id.txtWelcome)).check(matches(withText(R.string.user_welcome_title)))
        onView(withId(R.id.recyclerSpotlight)).check(matches(isEnabled()))
        onView(withId(R.id.txtProducts)).check(matches(withText(R.string.txt_products)))
        onView(withId(R.id.imgCash)).check(matches(isEnabled()))
        onView(withId(R.id.recyclerProduct)).check(matches(isEnabled()))
    }

    @Test
    fun testToast() {
        onView(withId(R.id.imgCash)).perform(ViewActions.click())
        onView(withText(startsWith("Title: digio Cash")))
            .inRoot(withDecorView(not(rule.activity.window.decorView)))
            .check(matches(isDisplayed()))
    }

}