package br.com.duduvp.sempreittest.data.model.dto

import br.com.duduvp.sempreittest.data.model.Cash
import br.com.duduvp.sempreittest.data.model.Products
import br.com.duduvp.sempreittest.data.model.Spotlight

data class PojoDataDTO(val spotlight: List<Spotlight>, val products: List<Products>, val cash: Cash)