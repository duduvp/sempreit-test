package br.com.duduvp.sempreittest.data.model

data class Spotlight(val name: String, val bannerURL: String, val description: String)