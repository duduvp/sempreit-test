package br.com.duduvp.sempreittest.data.api

import br.com.duduvp.sempreittest.data.model.dto.PojoDataDTO
import retrofit2.Call
import retrofit2.http.GET

interface DigioApiDef {

    @GET("products")
    fun getProducts(): Call<PojoDataDTO>

}