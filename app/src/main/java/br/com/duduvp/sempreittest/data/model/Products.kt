package br.com.duduvp.sempreittest.data.model

data class Products(val imageURL: String, val name: String, val description: String)