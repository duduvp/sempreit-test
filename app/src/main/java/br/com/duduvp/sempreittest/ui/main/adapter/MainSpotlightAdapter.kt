package br.com.duduvp.sempreittest.ui.main.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import br.com.duduvp.sempreittest.R
import br.com.duduvp.sempreittest.data.model.Spotlight
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.main_adapter_spotlight.view.*

class MainSpotlightAdapter(private val properties: List<Spotlight>, private val context: Context) :
    RecyclerView.Adapter<MainSpotlightAdapter.ViewItemHolder>() {

    class ViewItemHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image: ImageView = view.imgSpotlight
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewItemHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.main_adapter_spotlight, parent, false)
        return ViewItemHolder(view)
    }

    override fun getItemCount(): Int {
        return properties.size
    }

    override fun onBindViewHolder(holder: ViewItemHolder, position: Int) {
        val spotlight = properties[position]
        Glide.with(context).load(spotlight.bannerURL).into(holder.image)
        holder.image.setOnClickListener {
            Toast.makeText(
                context,
                String.format("Name: %s\nDescription: %s", spotlight.name, spotlight.description),
                Toast.LENGTH_LONG
            ).show()
        }
    }
}