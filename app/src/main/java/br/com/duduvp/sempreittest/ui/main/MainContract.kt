package br.com.duduvp.sempreittest.ui.main

import android.graphics.drawable.Drawable
import br.com.duduvp.sempreittest.data.model.Cash
import br.com.duduvp.sempreittest.ui.main.adapter.MainProductAdapter
import br.com.duduvp.sempreittest.ui.main.adapter.MainSpotlightAdapter

interface MainContract {

    interface View {
        fun initViews()
        fun loadData()
        fun showCash(cash : Cash)
        fun showProduct(productAdapter: MainProductAdapter)
        fun showSpotlight(spotlightAdapter: MainSpotlightAdapter)
        fun showError(message: String?)
        fun showProgress(show: Boolean)

    }

    interface Presenter {
        fun checkConnection(): Boolean?
        fun searchData()
    }

}