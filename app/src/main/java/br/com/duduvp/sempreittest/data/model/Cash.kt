package br.com.duduvp.sempreittest.data.model

data class Cash(val bannerURL: String, val description: String, val title: String)