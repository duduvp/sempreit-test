package br.com.duduvp.sempreittest.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.duduvp.sempreittest.R
import br.com.duduvp.sempreittest.data.model.Cash
import br.com.duduvp.sempreittest.ui.main.adapter.MainProductAdapter
import br.com.duduvp.sempreittest.ui.main.adapter.MainSpotlightAdapter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainContract.View {

    private lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
        loadData()
    }

    override fun initViews() {
        presenter = MainPresenter(this)
    }

    override fun loadData() {
        presenter.searchData()
    }

    override fun showCash(cash: Cash) {
        Glide.with(this).load(cash.bannerURL).into(imgCash)
        imgCash.contentDescription = cash.description
        imgCash.setOnClickListener {
            Toast.makeText(
                this,
                String.format("Title: %s\nDescription: %s", cash.title, cash.description),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    override fun showProduct(productAdapter: MainProductAdapter) {
        recyclerProduct.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        recyclerProduct.adapter = productAdapter
    }

    override fun showSpotlight(spotlightAdapter: MainSpotlightAdapter) {
        recyclerSpotlight.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        recyclerSpotlight.adapter = spotlightAdapter
    }

    override fun showError(message: String?) {
        Log.e(MainActivity::class.java.canonicalName, "Cannot load data! Details: $message")
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle("Warning!")
        builder.setMessage(message)
        builder.setPositiveButton(R.string.try_again) { dialog, which -> loadData() }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    override fun showProgress(show: Boolean) {
        when (show) {
            true -> progressBar.visibility = ProgressBar.VISIBLE
            else -> progressBar.visibility = ProgressBar.INVISIBLE
        }
    }

}
