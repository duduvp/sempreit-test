package br.com.duduvp.sempreittest.data.api

import br.com.duduvp.sempreittest.data.model.dto.PojoDataDTO
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DigioApi {

    private val service: DigioApiDef

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        service = retrofit.create(DigioApiDef::class.java)
    }

    fun loadProducts(): Call<PojoDataDTO> {
        return service.getProducts()
    }

}