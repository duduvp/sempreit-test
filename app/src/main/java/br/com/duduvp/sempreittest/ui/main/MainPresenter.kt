package br.com.duduvp.sempreittest.ui.main

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import br.com.duduvp.sempreittest.data.api.DigioApi
import br.com.duduvp.sempreittest.data.model.dto.PojoDataDTO
import br.com.duduvp.sempreittest.ui.main.adapter.MainProductAdapter
import br.com.duduvp.sempreittest.ui.main.adapter.MainSpotlightAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainPresenter(mainView: MainActivity) : MainContract.Presenter {

    private val view: MainActivity = mainView

    override fun searchData() {
        when (checkConnection()) {
            true -> {
                view.showProgress(true)
                val api = DigioApi()
                val request: Call<PojoDataDTO> = api.loadProducts()
                request.enqueue(object : Callback<PojoDataDTO> {
                    override fun onFailure(call: Call<PojoDataDTO>, t: Throwable) {
                        view.showError(t.message)
                    }

                    override fun onResponse(call: Call<PojoDataDTO>, response: Response<PojoDataDTO>) {
                        response.body()?.let { it ->
                            view.showCash(it.cash)
                            view.showSpotlight(MainSpotlightAdapter(it.spotlight, view))
                            view.showProduct(MainProductAdapter(it.products, view))
                        }
                    }

                })
                view.showProgress(false)
            }
            else -> {
                view.showError("WiFi or mobile data are disabled. Turn on and try again!")
            }
        }

    }

    override fun checkConnection(): Boolean? {
        val connectivityManager = view.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        return activeNetwork?.isConnected
    }

}